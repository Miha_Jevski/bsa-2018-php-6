@extends ('layouts.app')

@section('content')
    <!--Table-->
    <table class="table">

        <!--Table head-->
        <thead class="blue-grey lighten-4">
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Short Name</th>
            <th>Actual Course</th>
            <th>Date</th>
            <th>Active</th>
        </tr>
        </thead>
        <!--Table head-->

        <!--Table body-->
        <tbody>
        @foreach($currencies as $currency)
            <tr>
                <th scope="row">{{ $currency->getId() }}</th>
                <td>{{ $currency->getName() }}</td>
                <td>{{ $currency->getShortName() }}</td>
                <td>{{ $currency->getActualCourse() }}</td>
                <td>{{ $currency->getActualCourseDate()->format('d/m/Y') }}</td>
                <td>{{ $currency->isActive() }}</td>
            </tr>
        @endforeach
        </tbody>
        <!--Table body-->

    </table>
    <!--Table-->

@endsection