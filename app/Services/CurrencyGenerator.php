<?php

namespace App\Services;

class CurrencyGenerator
{
    public static function generate(): array
    {
        $currencies = [];
        for ($i = 1; $i <= 10; $i++) {
            $currencies[] = new Currency(
                $i,
                'coin' . $i,
                'shortName' . $i,
                rand(100,10000),
                new \DateTime(),//todo
                (bool)rand(0,1)
            );
        }
        return $currencies;
    }
}