<?php

namespace App\Http\Controllers\Admin;

use App\Services\CurrencyRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CurrencyController extends Controller
{
    public function index()
    {
        $repository = app()->make(CurrencyRepositoryInterface::class);
        $currencies = $repository->findAll();
        return view('currency.index', compact('currencies'));
    }
}
