<?php

namespace App\Http\Controllers\Api\Admin;

use App\Services\Currency;
use App\Services\CurrencyPresenter;
use App\Services\CurrencyRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CurrencyController extends Controller
{
    private $repository;

    public function __construct()
    {
        $this->repository = app()->make(CurrencyRepositoryInterface::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currencies = $this->repository->findAll();
        $output = [];
        foreach ($currencies as $currency){
            $output[] = CurrencyPresenter::present($currency);
        }
        return response()->json($output);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = 11; //todo
        $name = $request->get('name');
        $shortName = $request->get('short_name');
        $actualCourse = $request->get('actual_course');
        $actualCourseDate = $request->get('actual_course_date');
        $active = $request->get('active');

        $this->repository->save(new Currency($id, $name, $shortName, $actualCourse, $actualCourseDate, $active));

        $currency = $this->repository->findById($id);
        return response()->json(CurrencyPresenter::present($currency));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $currency = $this->repository->findById($id);
        if(!$currency) return response()->json([], 404);
        return response()->json(CurrencyPresenter::present($currency));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currency = $this->repository->findById($id);
        if(!$currency) return response()->json([], 404);
        $currency->setActualCourse($request->get('actual_course'));
        $currency->setActualCourseDate($request->get('actual_course_date'));
        return response()->json(CurrencyPresenter::present($currency));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currency = $this->repository->findById($id);
        if(!$currency) return response()->json([], 404);
        $this->repository->delete($currency);
        return response()->json([], 200);
    }
}
