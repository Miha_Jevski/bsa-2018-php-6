<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\CurrencyPresenter;
use App\Services\CurrencyRepositoryInterface;

class CurrencyController extends Controller
{
    public function index()
    {
        $repository = app()->make(CurrencyRepositoryInterface::class);
        $activeCurrencies = $repository->findActive();
        $output = [];
        foreach ($activeCurrencies as $currency){
            $output[] = CurrencyPresenter::present($currency);
        }
        return response()->json($output);
    }

    public function show($id)
    {
        $repository = app()->make(CurrencyRepositoryInterface::class);
        $currency = $repository->findById($id);
        if(!$currency) return response()->json([], 404);
        return response()->json(CurrencyPresenter::present($currency));
    }
}
