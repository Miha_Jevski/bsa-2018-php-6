<?php

namespace App\Providers;

use App\Repositories\InMemoryCurrencyRepository;
use App\Services\{CurrencyGenerator, CurrencyRepositoryInterface};
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->bind(CurrencyRepositoryInterface::class, DBCurrencyRepository::class);

        $this->app->bind(CurrencyRepositoryInterface::class, function(){
            return new InMemoryCurrencyRepository(CurrencyGenerator::generate());
        });
    }
}
