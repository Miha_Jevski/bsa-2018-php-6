<?php

namespace App\Repositories;

use App\Services\Currency;
use App\Services\CurrencyRepositoryInterface;

class InMemoryCurrencyRepository implements CurrencyRepositoryInterface
{
    private $currencies;

    public function __construct(array $currencies)
    {
        $this->currencies = $currencies;
    }

    /**
     * @return Currency[]
     */
    public function findAll(): array
    {
        return $this->currencies;
    }

    public function findActive(): array
    {
        $activeCurrencies =[];
        foreach ($this->currencies as $value){
            if($value->isActive()) $activeCurrencies[] = $value;
        }
        return $activeCurrencies;
    }

    public function findById(int $id): ?Currency
    {
        foreach ($this->currencies as $value){
            if($value->getId() == $id) return $value;
        }
        return null;
    }

    public function save(Currency $currency): void
    {
        $this->currencies[] = $currency;
    }

    public function delete(Currency $currency): void
    {
        foreach ($this->currencies as $key=>$value){
            if($value == $currency) {
                unset($this->currencies[$key]);
                break;
            }
        }
    }
}