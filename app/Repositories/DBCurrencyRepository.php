<?php

namespace App\Repositories;

use App\Services\Currency;
use App\Services\CurrencyRepositoryInterface;

class DBCurrencyRepository implements CurrencyRepositoryInterface
{
    /**
     * @return Currency[]
     */
    public function findAll(): array
    {
        // TODO: Implement findAll() method.
    }

    public function findActive(): array
    {
        // TODO: Implement findActive() method.
    }

    public function findById(int $id): ?Currency
    {
        // TODO: Implement findById() method.
    }

    public function save(Currency $currency): void
    {
        // TODO: Implement save() method.
    }

    public function delete(Currency $currency): void
    {
        // TODO: Implement delete() method.
    }
}